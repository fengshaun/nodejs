var fs = require("fs");

var peach = function(o) {
  console.trace("traced");
  console.dir(o);
}

var bowser = function(cb) {
  fs.readFile(process.argv[2], {encoding: "utf8"}, cb);
}

var koopa = function(err, f) {
  if (err) {
    console.error(err);
    return;
  }

  peach(JSON.parse(f));
}

bowser(koopa);
