module.exports = foo;

function foo(cb) {
  var server = require("http").createServer(function(req, res) {
    res.end("hello");
  });

  var repl = require("replify")("bug-clinic", {__message: "REPLs are neat"});

  server.listen(8080, function() {
    cb(server, repl);
  });
}
