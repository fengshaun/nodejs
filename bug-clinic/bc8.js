var trace = require("jstrace");
var http = require("http");
var url = require("url");

var server = http.createServer(function (req, res) {
  trace("request:start", {url: req.url});
  var parsed = url.parse(req.url);
  var payload = "";

  if (parsed.pathname == "/prognosis" && req.method === "GET") {
    res.statusCode = 200;
    payload = JSON.stringify({ok: true});
  } else {
    res.statusCode = 404;
    payload = JSON.stringify({error: "notfound"});
  }

  res.end(payload);
  trace("request:end", {statusCode: res.statusCode, body: payload});
});

server.listen(9999, function () {
  console.error("up");
});
