var http = require("http");
var fs = require("fs");

var port = process.argv[2];
var filename = process.argv[3];

var s = http.createServer(function(req, resp) {
  // pipe() calls end() somehow
  fs.createReadStream(filename).pipe(resp);
});

s.listen(port);

