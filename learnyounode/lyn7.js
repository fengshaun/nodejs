var http = require("http");
var catstream = require("concat-stream");

var url = process.argv[2];

http.get(url, function(res) {
  res.setEncoding("utf8");
  res.pipe(catstream(function(err, data) {
    if (err) throw err;

    console.log(data.length);
    console.log(data);
  }));
});
