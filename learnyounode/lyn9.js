var catstream = require("concat-stream");
var http = require("http");

var urls = process.argv.slice(2),
    count = urls.length,
    d = [];

function printData() {
  d.forEach(function(i) {
    console.log(i.data);
  });
}

function Get(url) {
  var _page = this;
  _page.url = url;
  _page.data = "";

  http.get(url, function(res) {
    res.setEncoding("utf8");

    res.on("error", function(err) {
      console.log(err);
    });

    res.pipe(catstream(function(data) {
      _page.data = data;
      if (--count === 0) {
        printData();
      }
    }));
  });
}

for (var i = 0; i < urls.length; i++) {
  d.push(new Get(urls[i]));
}

