var http = require("http");
var url = require("url");

var port = process.argv[2];

function parseTime(t) {
  var date = new Date(t);
  return {hour: date.getHours(),
          minute: date.getMinutes(),
          second: date.getSeconds()};
}

function getUnixTime(t) {
  var date = new Date(t);
  return {unixtime: date.getTime()};
}

var s = http.createServer(function(req, resp) {
  if (req.method != 'GET') {
    return res.end("method " + req.method + " not supported. Supported: 'GET'");
  }

  var u = url.parse(req.url, true);

  if (u.pathname === "/api/unixtime") {
    resp.writeHead(200, {'Content-Type': 'application/json'});
    resp.end(JSON.stringify(getUnixTime(u.query["iso"])));
  } else if (u.pathname === "/api/parsetime") {
    resp.writeHead(200, {'Content-Type': 'application/json'});
    resp.end(JSON.stringify(parseTime(u.query["iso"])));
  } else {
    resp.writeHead(400);
    resp.end();
  }

});

s.listen(port);

