var mod = require("./lyn6_mod");

var dir = process.argv[2];
var ext = process.argv[3];

mod(dir, ext, function(err, data) {
  if (err) throw err;

  data.forEach(function(f) {
    console.log(f);
  });
});
