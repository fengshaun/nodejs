var http = require("http");
var map = require("through2-map"); // map on streams

var port = process.argv[2];

var s = http.createServer(function(req, resp) {
  if (req.method != 'POST') {
    return res.end("method " + req.method + " not supported");
  }

  req.setEncoding("utf8");

  req.pipe(map(function(chunk) {
    // not sure why setEncoding is not doing the conversion here
    return chunk.toString().toUpperCase();
  })).pipe(resp);
});

s.listen(port);
