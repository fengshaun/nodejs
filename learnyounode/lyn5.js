var fs = require("fs");
var path = require("path");

var dir = process.argv[2];
var extension = process.argv[3];

fs.readdir(dir, function (err, data) {
  if (err) throw err;

  for (i = 0; i < data.length; i++) {
    if (extension === path.extname(data[i]).slice(1)) {
      console.log(data[i]);
    }
  }
});
