var fs = require("fs");
var path = require("path");

module.exports = function(dir, ext, callback) {
  l = [];
  fs.readdir(dir, function(err, data) {
    if (err) { return callback(err); }

    data.forEach(function(f) {
      if (path.extname(f).slice(1) == ext) {
        l.push(f);
      }
    });

    callback(null, l);
  });
}

