var net = require("net");
var strftime = require("strftime");

var port = process.argv[2];

function getTime() {
  return strftime("%F %H:%M", new Date());
}

var s = net.createServer(function(sock) {
  sock.write(getTime() + "\n");
  sock.end();
});

s.listen(port);
